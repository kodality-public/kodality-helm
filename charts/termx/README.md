# TermX

TermX is an open-source terminology server based on CTS2 and FHIR standards within adapters for the import of external classifications.

## Introduction

This chart bootstraps a [Kodality Termx](https://gitlab.com/kodality/terminology) deployment on a [Kubernetes](https://kubernetes.io)
cluster using the [Helm](https://helm.sh) package manager.

## Optional

- Snowstorm SNOMED server
- MinIO

### Keycloak

TermX can work with any OpenID Connect provider. As an option you can
use [Bitnami Keycloak Helm chart](https://github.com/bitnami/charts/tree/master/bitnami/keycloak)

### Installing PostgreSQL server

Postgresql can be installed via [Bitnami PostgreSQL Helm Chart](https://github.com/bitnami/charts/tree/master/bitnami/postgresql). It is required to create
database and base users. Remember to change `test` passwords.

```postgresql
create role termserver_admin login password 'test' nosuperuser inherit nocreatedb createrole noreplication;
create role termserver_app login password 'test' nosuperuser inherit nocreatedb createrole noreplication;
create role termserver_viewer nologin nosuperuser inherit nocreatedb nocreaterole noreplication;
create database termserver with owner = termserver_admin encoding = 'UTF8' tablespace = pg_default connection limit = -1;
grant temp on database termserver to termserver_app;
grant connect on database termserver to termserver_app;
create extension if not exists hstore schema public;
```

## Installing the Chart

To install the chart with the release name `my-termx-release`:

```console
$ helm repo add kodality https://kexus.kodality.com/repository/helm
$ helm install my-termx-release kodality/termx
```

These commands deploy TermX on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be
configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-termx-release` release:

```console
$ helm delete my-termx-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release. Remove also the chart using `--purge` option:

```console
$ helm delete --purge my-termx-release
```

## Parameters

### PostgreSQL parameters

| Name                      | Description                 | Value                                                           |
|---------------------------|-----------------------------|-----------------------------------------------------------------|
| `postgres.url`            | Postgres JDBC URL           | `"jdbc:postgresql://termx-postgres-postgresql:5432/termserver"` |
| `postgres.dbPoolSize`     | DB connection pool size     | `10`                                                            |
| `postgres.admin.password` | DB owner's password (admin) | `""`                                                            |
| `postgres.app.password`   | DB user's password (app)    | `""`                                                            |

### OAuth parameters

| Name           | Description    | Value                                                                          |
|----------------|----------------|--------------------------------------------------------------------------------|
| `oauth.jwks`   | OAuth JWKS URL | `"https://auth.kodality.dev/realms/terminology/protocol/openid-connect/certs"` |
| `oauth.issuer` | OAuth issuer   | `"https://auth.kodality.dev/realms/terminology`                                |
| `oauth.client` | OAuth client   | `"term-client"`                                                                |

### Web parameters

| Name                          | Description                                                                                                                          | Value                             | Optional |
|-------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------|----------|
| `sevices.web.image`           | Docker image                                                                                                                         | `"docker.kodality.com/termx-web"` |          |
| `sevices.web.version`         | Docker image version                                                                                                                 | `"latest"`                        |          |
| `sevices.web.limits.cpu`      | CPU resource limit                                                                                                                   | `100m`                            |          |
| `sevices.web.limits.memory`   | Memory resource limit                                                                                                                | `128Mi`                           |          |
| `sevices.web.requests.cpu`    | Requested CPU resource                                                                                                               | `100m`                            |          |
| `sevices.web.requests.memory` | Requested memory resource                                                                                                            | `64Mi`                            |          |
| `sevices.web.extraEnvVars`    | Additional environment variables to set ([environment variables](https://termx.kodality.dev/wiki/termx-tutorial/installation-guide)) | `[]`                              | ✔️       |

### Server parameters

#### Basic

| Name                             | Description                                                                                                                          | Value                                | Optional |
|----------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|----------|
| `sevices.server.image`           | Docker image                                                                                                                         | `"docker.kodality.com/termx-server"` |          |
| `sevices.server.version`         | Docker image version                                                                                                                 | `"latest"`                           |          |
| `sevices.server.limits.cpu`      | CPU resource limit                                                                                                                   | `2`                                  |          |
| `sevices.server.limits.memory`   | Memory resource limit                                                                                                                | `2048Mi`                             |          |
| `sevices.server.requests.cpu`    | Requested CPU resource                                                                                                               | `100m`                               |          |
| `sevices.server.requests.memory` | Requested memory resource                                                                                                            | `512Mi`                              |          |
| `sevices.server.applicationConf` | String that contains the full Micronaut configuration in JSON format                                                                 | `""`                                 | ✔️       |
| `sevices.server.extraEnvVars`    | Additional environment variables to set ([environment variables](https://termx.kodality.dev/wiki/termx-tutorial/installation-guide)) | `[]`                                 | ✔️       |

#### Snowstorm

| Name                                 | Description                            | Value                               | Optional |
|--------------------------------------|----------------------------------------|-------------------------------------|----------|
| `sevices.server.snowstorm.enabled`   | Set to true to enable Snowstorm        | `"false"`                           |          |
| `sevices.server.snowstorm.url`       | Server base URL                        | `"https://snowstorm.kodality.dev/"` |          |
| `sevices.server.snowstorm.user`      | Basic auth username                    | `""`                                | ✔️       |
| `sevices.server.snowstorm.password`  | Basic auth password                    | `""`                                | ✔️       |
| `sevices.server.snowstorm.branch`    | Default SNOMED edition                 | `"MAIN"`                            |          |
| `sevices.server.snowstorm.namespace` | Default SNOMED CT Namespace Identifier | `"1000265"`                         |          |

#### Users provider parameters

| Name                                    | Description                                        | Value                                                                    |
|-----------------------------------------|----------------------------------------------------|--------------------------------------------------------------------------|
| `sevices.server.users.enabled`          | Set to true to start providing users from Keycloak | `"false"`                                                                |
| `sevices.server.users.keycloakAdminApi` | Keycloak API URL                                   | `"https://auth.kodality.dev/admin/realms/terminology"`                   |
| `sevices.server.users.oauthUrl`         | OAuth URL                                          | `"https://auth.kodality.dev/realms/terminology/protocol/openid-connect"` |
| `sevices.server.users.client`           | OAuth client                                       | `"term-service"`                                                         |
| `sevices.server.users.secret`           | OAuth client secret                                | `""`                                                                     |

### MinIO parameters

| Name                             | Description                 | Value     |
|----------------------------------|-----------------------------|-----------|
| `sevices.server.minio.enabled`   | Set to true to enable MinIO | `"false"` |
| `sevices.server.minio.url`       | Keycloak API URL            | `""`      |
| `sevices.server.minio.accessKey` | OAuth URL                   | `""`      |
| `sevices.server.minio.secretKey` | OAuth client                | `""`      |

### CHEF parameters

| Name                           | Description               | Value                            |
|--------------------------------|---------------------------|----------------------------------|
| `sevices.chef.image`           | Docker image              | `"docker.kodality.com/fsh-chef"` |
| `sevices.chef.version`         | Docker image version      | `"latest"`                       |
| `sevices.chef.limits.cpu`      | CPU resource limit        | `1`                              |
| `sevices.chef.limits.memory`   | Memory resource limit     | `2048Mi`                         |
| `sevices.chef.requests.cpu`    | Requested CPU resource    | `100m`                           |
| `sevices.chef.requests.memory` | Requested memory resource | `256Mi`                          |

### PlantUML parameters

| Name                               | Description               | Value                        |
|------------------------------------|---------------------------|------------------------------|
| `sevices.plantuml.image`           | Docker image              | `"plantuml/plantuml-server"` |
| `sevices.plantuml.version`         | Docker image version      | `"latest"`                   |
| `sevices.plantuml.limits.cpu`      | CPU resource limit        | `1`                          |
| `sevices.plantuml.limits.memory`   | Memory resource limit     | `512Mi`                      |
| `sevices.plantuml.requests.cpu`    | Requested CPU resource    | `100m`                       |
| `sevices.plantuml.requests.memory` | Requested memory resource | `256Mi`                      |

### FML editor parameters

| Name                                | Description               | Value                                    |
|-------------------------------------|---------------------------|------------------------------------------|
| `sevices.fmlEditor.image`           | Docker image              | `"docker.kodality.com/termx-fml-editor"` |
| `sevices.fmlEditor.version`         | Docker image version      | `"latest"`                               |
| `sevices.fmlEditor.limits.cpu`      | CPU resource limit        | `1`                                      |
| `sevices.fmlEditor.limits.memory`   | Memory resource limit     | `512Mi`                                  |
| `sevices.fmlEditor.requests.cpu`    | Requested CPU resource    | `100m`                                   |
| `sevices.fmlEditor.requests.memory` | Requested memory resource | `256Mi`                                  |

## Add extra environment variables

In case you want to add extra environment variables (useful for advanced operations like custom init scripts), you can use the `extraEnvVars` property.

```yaml
extraEnvVars:
  - name: ENV_VAR_NAME
    value: ENV_VAR_VALUE
```

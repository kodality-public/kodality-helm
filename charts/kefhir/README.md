<!--- app-name: Kodality Terminology Server -->

# KeFHIR (FHIR server)

KeFHIR is an open-source FHIR server.

## TL;DR

```console
$ helm repo add kodality https://kexus.kodality.com/helm
$ helm install my-kefhir-release kodality/kefhir
```

## Introduction

This chart bootstraps a [Kodality Terminology Server](https://gitlab.com/kodality-public/terminology-server) deployment on a [Kubernetes](https://kubernetes.io)
cluster using the [Helm](https://helm.sh) package manager.

## Prerequisites

- Kubernetes 1.19+
- Helm 3.2.0+
- PostgreSQL 14+


### Installing PostgreSQL server

Postgresql can be installed via e [Bitnami PostgreSQL Helm Chart] (https://github.com/bitnami/charts/tree/master/bitnami/postgresql). It is required to create 
database and base users. Remember to change 'test' passwords.


## Installing the Chart

To install the chart with the release name `my-kefhir-release`:

```console
$ helm repo add kodality https://kexus.kodality.com/repository/helm
$ helm install my-kefhiir-release kodality/kefhir
```

These commands deploy KTS on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be
configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-kefhiir-release` release:

```console
$ helm delete my-kefhiir-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release. Remove also the chart using `--purge` option:

```console
$ helm delete --purge  my-kefhiir-release
```

## Parameters

### Kefhir deployment parameters

| Name                               | Description               | Value                                      |
|------------------------------------|---------------------------|--------------------------------------------|
| `sevices.kefhir.image`             | KeFHIR backend image      | `"docker.kodality.com/kefhir-demo"` |
| `sevices.kefhir.replicas`       | Numbe of replicas         | `1`                                        |
| `sevices.kefhir.limits.cpu`     | CPU resource limit        | `2`                                        |
| `sevices.kefhir.limits.memory`  | Memory resouce limit      | `2048Mi`                                   |
| `sevices.kefhir.requests.cpu`   | Requested CPU resource    | `100m`                                     |
| `sevices.kefhir.requests.memory` | Requested memory resource | `512Mi`                                    |


### FHIR parameters

| Name                  | Description                                | Value         |
|-----------------------|--------------------------------------------|---------------|
| `fhir.definitionsUrl`        | FHIR definitions url                               | `"https://kexus.kodality.com/repository/store-public/kefhir/definitions.json.zip"` |

### Ingress parameters

| Name                  | Description                                | Value         |
|-----------------------|--------------------------------------------|---------------|
| `ingress.host`        | KeFHIR host                                | `"kts.local"` |
| `ingress.contextPath` | KeFHIR context path for ingress controller | `"/"`         |
| `ingress.tls`         | TLS configuration                          | `[]`          |

### PostgreSQL parameters

| Name                      | Description             | Value                                                 |
|---------------------------|-------------------------|-------------------------------------------------------|
| `postgres.url`            | Postgres JDBC URL       | `"jdbc:postgresql://postgres.default:5432/kefhir"` |
| `postgres.dbPoolSize`     | DB connection pool size | `10`                                                  |
| `postgres.admin.user`     | DB owner's user  name   | `kefhir_admin`                                        |
| `postgres.admin.password` | DB owner's password     | `test`                                                |
| `postgres.user.user`      | DB user's username      | `kefhir_app`                                          |
| `postgres.user.password`  | DB user's password      | `test`                                                |

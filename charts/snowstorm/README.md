<!--- app-name:  Snowstorm SnomedCT terminology server -->

# Snowstorm SnomedCT terminology server

## TL;DR

```console
$ helm repo add kodality https://kexus.kodality.com/repository/helm
$ helm install snowstorm kodality/snowstorm
```

## Introduction

This chart bootstraps a [Snowstorm SnomdeCT](https://github.com/IHTSDO/snowstorm) deployment on a [Kubernetes](https://kubernetes.io)
cluster using the [Helm](https://helm.sh) package manager. Chart are meant for testing purposee only and not suitable for production use

## Prerequisites

- Kubernetes 1.19+
- Helm 3.2.0+


## Parameters

### Volume parameters

| Name                                | Description               | Value      |
|-------------------------------------|---------------------------|------------|
| `volume.storage`                    | Elastic Storage size      | `"4Gi"`    |
| `volume.storageClass`               | Elastic Storage cllass    | `hostpath` |
